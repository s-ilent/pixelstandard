# PixelStandard

A modified version of Unity's Standard shader that performs sharpened pixellated sampling. Unlike nearest-neighbour texture filtering, this lets you use anisotropic filtering and trilinear filtering without artifacts, and looks clean in VR without shimmering.

## Installation

Download the repository. Then place the Shader/ folder with the shader into your Assets/ directory.

## Usage

This shader comes with 3 variants.
* **PixelUnlit**, which is an Unlit shader that implements the sharp filtering. You can set it to Opaque, Cutout, or Fade/Transparent blend modes. 
* **PixelStandard**, which is a modified Standard shader that implements the sharp filtering. It has all functionality present in Standard, and applies the filtering to all maps.
* A Specular variant of Standard.

Make sure textures are set to any **linear** filtering mode. Then apply the appropriate shader and off you go!

This shader supports lightmapping. Note that lightmaps will not receive sharp filtering. 

## License?
MIT license, same as the original.